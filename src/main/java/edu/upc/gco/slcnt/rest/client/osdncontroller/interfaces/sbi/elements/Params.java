package edu.upc.gco.slcnt.rest.client.osdncontroller.interfaces.sbi.elements;

public class Params {

	private String testParam;
	
	public Params() {
		
		testParam = "test";
	}

	public String getTestParam() {
		return testParam;
	}

	public void setTestParam(String testParam) {
		this.testParam = testParam;
	}
}