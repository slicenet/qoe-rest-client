package edu.upc.gco.slcnt.rest.client.nmro.interfaces.cslcm.messages;

import it.nextworks.nfvmano.libs.ifa.common.InterfaceMessage;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.MalformattedElementException;

public class CreateCsIdentifierRequest implements InterfaceMessage {

	private String csName;
	private String csDescription;
	private String tenantId;
	
	public CreateCsIdentifierRequest()
	{
		
	}
	
	public CreateCsIdentifierRequest(String csName, String csDescription, String tenantId)
	{
		this.csName = csName;
		this.csDescription = csDescription;
		this.tenantId = tenantId;
	}
	
	public String getCsName() {
		return csName;
	}


	public String getCsDescription() {
		return csDescription;
	}


	public String getTenantId() {
		return tenantId;
	}


	@Override
	public void isValid() throws MalformattedElementException {
		if (csName == null)
			throw new MalformattedElementException("Create CS ID request without Name");
		if (tenantId == null)
			throw new MalformattedElementException("Create CS ID request without Tenant");
	}

}
