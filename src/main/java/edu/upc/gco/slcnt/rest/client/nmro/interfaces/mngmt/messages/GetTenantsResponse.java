package edu.upc.gco.slcnt.rest.client.nmro.interfaces.mngmt.messages;

import java.util.ArrayList;
import java.util.List;

import it.nextworks.nfvmano.libs.ifa.common.InterfaceMessage;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.MalformattedElementException;

public class GetTenantsResponse implements InterfaceMessage {

	private List<String> tenantIds = new ArrayList<String>();
	
	public GetTenantsResponse ()
	{
		
	}
	
	public GetTenantsResponse (List<String> tenantIds)
	{
		this.tenantIds = tenantIds;
	}

	public List<String> getTenantIds() {
		return tenantIds;
	}

	@Override
	public void isValid() throws MalformattedElementException
	{
		
	}
}
