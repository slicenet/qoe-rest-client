package edu.upc.gco.slcnt.rest.client.osm.lcm.nsiManagement;

import java.time.LocalDateTime;

import org.springframework.http.ResponseEntity;

public interface NSIManagementInterface {

	ResponseEntity<?> createNsiIdentifier(String nsName, String nsdId, String nsDescription, String vimAccountId);
	ResponseEntity<?> getNsiInfo(String nsiId);
	ResponseEntity<?> getNsiInfoList();
	ResponseEntity<?> instantiateNsi(String nsiId, String nsName, String nsdId, String vimAccountId);
	ResponseEntity<?> teminateNsi(String nsiId, LocalDateTime terminationTime);
	ResponseEntity<?> deleteNsInstance(String nsiId);
	ResponseEntity<?> getVnfiInfoList();
	ResponseEntity<?> getVnfiInfo (String vnfiId);
}
