package edu.upc.gco.slcnt.rest.client.osm.model.nsiManagement;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Interface {

	@JsonProperty("external_name")
	private String externalName;
	
	@JsonProperty("instance_net_id")
	private String instanceNetId;
	
	@JsonProperty("internal_name")
	private String internalName;
	
	@JsonProperty("ip_address")
	private String ipAddress;
	
	@JsonProperty("mac_address")
	private String macAddress;
	
	@JsonProperty("sdn_port_id")
	private String sdnPortId;
	
	@JsonProperty("type")
	private String type;		//mgmt
	
	@JsonProperty("vim_info")
	private String vimInfoStr;		// JSON format String
	private VimInfo vimInfo;
	
	@JsonProperty("vim_interface_id")
	private String vimInterfaceId;
	
	@Transient
    private Map<String, Object> otherProperties = new HashMap<String, Object>();

	public String getExternalName() {
		return externalName;
	}

	public void setExternalName(String externalName) {
		this.externalName = externalName;
	}

	public String getInstanceNetId() {
		return instanceNetId;
	}

	public void setInstanceNetId(String instanceNetId) {
		this.instanceNetId = instanceNetId;
	}

	public String getInternalName() {
		return internalName;
	}

	public void setInternalName(String internalName) {
		this.internalName = internalName;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}

	public String getSdnPortId() {
		return sdnPortId;
	}

	public void setSdnPortId(String sdnPortId) {
		this.sdnPortId = sdnPortId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public /*VimInfo*/String getVimInfoStr() {
		return vimInfoStr;
	}

	public void setVimInfoStr(/*VimInfo*/String vimInfo) {
		this.vimInfoStr = vimInfo;
	}
	
	public void setVimInfo (String vimInfoStr)
	{
		ObjectMapper mapper = new ObjectMapper();
		this.vimInfo = null;
		try {
			this.vimInfo = mapper.readValue(vimInfoStr, VimInfo.class);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public VimInfo getVimInfo()
	{
		setVimInfo (this.vimInfoStr);
		return vimInfo;
	}

	public String getVimInterfaceId() {
		return vimInterfaceId;
	}

	public void setVimInterfaceId(String vimInterfaceId) {
		this.vimInterfaceId = vimInterfaceId;
	}
	
	@JsonAnyGetter
    public Map<String, Object> any() {
        return otherProperties;
    }

    @JsonAnySetter
    public void set(String name, Object value) {
        otherProperties.put(name, value);
    }
}
