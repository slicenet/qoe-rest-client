package edu.upc.gco.slcnt.rest.client.osdncontroller.interfaces.sbi.elements;

public class Link {

	private String linkId;
	private Node src;
	private Port srcPort;
	private Node dst;
	private Port dstPort;
	private String bw;
	private String delay;
	private String wl;
	
	public Link(String linkId, Node src, Port srcPort, Node dst, Port dstPort, String bw, String delay, String wl) {
		
		this.linkId = linkId;
		this.src = src;
		this.srcPort = srcPort;
		this.dst = dst;
		this.dstPort = dstPort;
		this.bw = bw;
		this.delay = delay;
		this.wl = wl;
	}

	public String getLinkId() {
		return linkId;
	}

	public void setLinkId(String linkId) {
		this.linkId = linkId;
	}

	public Node getSrc() {
		return src;
	}

	public void setSrc(Node src) {
		this.src = src;
	}

	public Port getSrcPort() {
		return srcPort;
	}

	public void setSrcPort(Port srcPort) {
		this.srcPort = srcPort;
	}

	public Node getDst() {
		return dst;
	}

	public void setDst(Node dst) {
		this.dst = dst;
	}

	public Port getDstPort() {
		return dstPort;
	}

	public void setDstPort(Port dstPort) {
		this.dstPort = dstPort;
	}

	public String getBw() {
		return bw;
	}

	public void setBw(String bw) {
		this.bw = bw;
	}

	public String getDelay() {
		return delay;
	}

	public void setDelay(String delay) {
		this.delay = delay;
	}

	public String getWl() {
		return wl;
	}

	public void setWl(String wl) {
		this.wl = wl;
	}
}