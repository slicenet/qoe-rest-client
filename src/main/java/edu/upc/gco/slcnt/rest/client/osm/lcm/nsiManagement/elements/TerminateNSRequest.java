package edu.upc.gco.slcnt.rest.client.osm.lcm.nsiManagement.elements;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TerminateNSRequest {

	/*@JsonProperty("terminationTime")
	private LocalDateTime terminationTime;

	public LocalDateTime getTerminationTime() {
		return terminationTime;
	}

	public void setTerminationTime(LocalDateTime terminationTime) {
		this.terminationTime = terminationTime;
	}*/
	@JsonProperty("timeout_ns_terminate")
	private int timeoutNsTerminate;
	
	@JsonProperty("autoremove")
	private boolean autoremove;
	
	@JsonProperty("skip_terminate_primitives")
	private boolean skipTerminatePrimitives;

	public int getTimeoutNsTerminate() {
		return timeoutNsTerminate;
	}

	public void setTimeoutNsTerminate(int timeoutNsTerminate) {
		this.timeoutNsTerminate = timeoutNsTerminate;
	}

	public boolean isAutoremove() {
		return autoremove;
	}

	public void setAutoremove(boolean autoremove) {
		this.autoremove = autoremove;
	}

	public boolean isSkipTerminatePrimitives() {
		return skipTerminatePrimitives;
	}

	public void setSkipTerminatePrimitives(boolean skipTerminatePrimitives) {
		this.skipTerminatePrimitives = skipTerminatePrimitives;
	}
}
