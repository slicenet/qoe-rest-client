package edu.upc.gco.slcnt.rest.client.cp.cpsr.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CPSRegistrationData {
	
	@JsonProperty("heartBeatTimer")
	private int heartBeatTimer;
	@JsonProperty("cpSProfile")
	private CPSProfile profile;
	
	public int getHeartBeatTimer() {
		return heartBeatTimer;
	}
	public void setHeartBeatTimer(int heartBeatTimer) {
		this.heartBeatTimer = heartBeatTimer;
	}
	public CPSProfile getProfile() {
		return profile;
	}
	public void setProfile(CPSProfile profile) {
		this.profile = profile;
	}
}
