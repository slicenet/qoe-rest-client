package edu.upc.gco.slcnt.rest.client.osm.model.nsiManagement;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;

public class VNF {

	@JsonProperty("datacenter_id")
	private String datacenterId;
	
	@JsonProperty("datacenter_tenant_id")
	private String datacenterTenantId;
	
	@JsonProperty("ip_address")
	private String ipAddress;				//floating;private
	
	@JsonProperty("member_vnf_index")
	private String memberVnfIndex;
	
	@JsonProperty("mgmt_access")
	//private MgmtAccess mgmtAccess;
	private String mgmtAccess;				// JSON format String!!
	
	@JsonProperty("sce_vnf_id")
	private String sceVnfId;
	
	@JsonProperty("uuid")
	private String uuid;
	
	@JsonProperty("vms")
	private List<VM> vms;
	
	@JsonProperty("vnf_id")
	private String vnfId;
	
	@JsonProperty("vnf_name")
	private String vnfName;
	
	@JsonProperty("vnfd_osm_id")
	private String vnfdOsmId;
	
	@Transient
    private Map<String, Object> otherProperties = new HashMap<String, Object>();

	public String getDatacenterId() {
		return datacenterId;
	}

	public void setDatacenterId(String datacenterId) {
		this.datacenterId = datacenterId;
	}

	public String getDatacenterTenantId() {
		return datacenterTenantId;
	}

	public void setDatacenterTenantId(String datacenterTenantId) {
		this.datacenterTenantId = datacenterTenantId;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getMemberVnfIndex() {
		return memberVnfIndex;
	}

	public void setMemberVnfIndex(String memberVnfIndex) {
		this.memberVnfIndex = memberVnfIndex;
	}

	public /*MgmtAccess*/String getMgmtAccess() {
		return mgmtAccess;
	}

	public void setMgmtAccess(/*MgmtAccess*/String mgmtAccess) {
		this.mgmtAccess = mgmtAccess;
	}

	public String getSceVnfId() {
		return sceVnfId;
	}

	public void setSceVnfId(String sceVnfId) {
		this.sceVnfId = sceVnfId;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public List<VM> getVms() {
		return vms;
	}

	public void setVms(List<VM> vms) {
		this.vms = vms;
	}

	public String getVnfId() {
		return vnfId;
	}

	public void setVnfId(String vnfId) {
		this.vnfId = vnfId;
	}

	public String getVnfName() {
		return vnfName;
	}

	public void setVnfName(String vnfName) {
		this.vnfName = vnfName;
	}

	public String getVnfdOsmId() {
		return vnfdOsmId;
	}

	public void setVnfdOsmId(String vnfdOsmId) {
		this.vnfdOsmId = vnfdOsmId;
	}
	
	@JsonAnyGetter
    public Map<String, Object> any() {
        return otherProperties;
    }

    @JsonAnySetter
    public void set(String name, Object value) {
        otherProperties.put(name, value);
    }
}
