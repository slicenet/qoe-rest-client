package edu.upc.gco.slcnt.rest.client.osm.model.nsiManagement.vnfi;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;

import edu.upc.gco.slcnt.rest.client.osm.model.common.AdminInfo;

public class VNFInstance {

	@JsonProperty("vnfd-id")
	private String vnfdId;
	@JsonProperty("created-time")
	private float created;
	@JsonProperty("additionalParamsForVnf")
	private String additionalParamsForVnf;	//TODO: Check format
	@JsonProperty("member-vnf-index-ref")
	private String memeberVnfIndexRef;
	@JsonProperty("_id")
	private String vnfiId;
	@JsonProperty("vdur")
	private List<VDUResource> vdurList;
	@JsonProperty("vim-account-id")
	private String vimAccountId;
	@JsonProperty("nsr-id-ref")
	private String nsrIdRef;
	@JsonProperty("vnfd-ref")
	private String vnfdRef;	// = vnfd name
	@JsonProperty("connection-point")
	private List<ConnectionPoint> cpList;
	@JsonProperty("_admin")
	private AdminInfo adminInfo;
	@JsonProperty("ip-address")
	private String ipAddress;
	@JsonProperty("id")
	private String id;	// = vnfiId (_id)
	
	@Transient
    private Map<String, Object> otherProperties = new HashMap<String, Object>();
	
	public String getVnfdId() {
		return vnfdId;
	}

	public void setVnfdId(String vnfdId) {
		this.vnfdId = vnfdId;
	}

	public float getCreated() {
		return created;
	}

	public void setCreated(float created) {
		this.created = created;
	}

	public String getAdditionalParamsForVnf() {
		return additionalParamsForVnf;
	}

	public void setAdditionalParamsForVnf(String additionalParamsForVnf) {
		this.additionalParamsForVnf = additionalParamsForVnf;
	}

	public String getMemeberVnfIndexRef() {
		return memeberVnfIndexRef;
	}

	public void setMemeberVnfIndexRef(String memeberVnfIndexRef) {
		this.memeberVnfIndexRef = memeberVnfIndexRef;
	}

	public String getVnfiId() {
		return vnfiId;
	}

	public void setVnfiId(String vnfiId) {
		this.vnfiId = vnfiId;
	}

	public List<VDUResource> getVdurList() {
		return vdurList;
	}

	public void setVdurList(List<VDUResource> vdurList) {
		this.vdurList = vdurList;
	}

	public String getVimAccountId() {
		return vimAccountId;
	}

	public void setVimAccountId(String vimAccountId) {
		this.vimAccountId = vimAccountId;
	}

	public String getNsrIdRef() {
		return nsrIdRef;
	}

	public void setNsrIdRef(String nsrIdRef) {
		this.nsrIdRef = nsrIdRef;
	}

	public String getVnfdRef() {
		return vnfdRef;
	}

	public void setVnfdRef(String vnfdRef) {
		this.vnfdRef = vnfdRef;
	}

	public List<ConnectionPoint> getCpList() {
		return cpList;
	}

	public void setCpList(List<ConnectionPoint> cpList) {
		this.cpList = cpList;
	}

	public AdminInfo getAdminInfo() {
		return adminInfo;
	}

	public void setAdminInfo(AdminInfo adminInfo) {
		this.adminInfo = adminInfo;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonAnyGetter
    public Map<String, Object> any() {
        return otherProperties;
    }

    @JsonAnySetter
    public void set(String name, Object value) {
        otherProperties.put(name, value);
    }
}
