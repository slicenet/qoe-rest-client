package edu.upc.gco.slcnt.rest.client.osdncontroller.interfaces.nbi.messages;

import edu.upc.gco.slcnt.rest.client.osdncontroller.interfaces.nbi.elements.SdnEndPoint;
import it.nextworks.nfvmano.libs.ifa.common.InterfaceMessage;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.MalformattedElementException;

public class CreateSdnIntentRequest implements InterfaceMessage {

	private SdnEndPoint endPointA;
	private SdnEndPoint endPointB;
	
	public CreateSdnIntentRequest ()
	{
		
	}
	
	public CreateSdnIntentRequest (SdnEndPoint endPointA, SdnEndPoint endPointB)
	{
		this.endPointA = endPointA;
		this.endPointB = endPointB;
	}
	
	public SdnEndPoint getEndPointA() {
		return endPointA;
	}

	public SdnEndPoint getEndPointB() {
		return endPointB;
	}

	@Override
	public void isValid() throws MalformattedElementException {
		if (endPointA == null || endPointB == null)
			throw new MalformattedElementException("Attempt to create an intent with missing End Points");
	}

}
