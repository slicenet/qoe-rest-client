package edu.upc.gco.slcnt.rest.client.nmro.interfaces.cslcm.elements;

import java.util.List;

import it.nextworks.nfvmano.libs.ifa.common.DescriptorInformationElement;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.MalformattedElementException;

public class CsInfo implements DescriptorInformationElement {

	private String csInstanceId;
	private String csName;
	private String csDescription;
	private List<String> ipopConnectionIds;
	
	public CsInfo () { }
	
	public CsInfo (String csInstanceId, String csName, String csDescription, List<String> ipopConnectionIds)
	{
		this.csInstanceId = csInstanceId;
		this.csName = csName;
		this.csDescription = csDescription;
		this.ipopConnectionIds = ipopConnectionIds;
	}
	
	public String getCsInstanceId() {
		return csInstanceId;
	}


	public void setCsInstanceId(String csInstanceId) {
		this.csInstanceId = csInstanceId;
	}


	public String getCsName() {
		return csName;
	}


	public void setCsName(String csName) {
		this.csName = csName;
	}


	public String getCsDescription() {
		return csDescription;
	}


	public void setCsDescription(String csDescription) {
		this.csDescription = csDescription;
	}


	public List<String> getIpopConnectionIds() {
		return ipopConnectionIds;
	}


	public void setIpopConnectionIds(List<String> ipopConnectionIds) {
		this.ipopConnectionIds = ipopConnectionIds;
	}
	
	@Override
	public void isValid() throws MalformattedElementException {
		if (csInstanceId == null)
			throw new MalformattedElementException("CS Info without CS instance ID");
		if (ipopConnectionIds == null)
			throw new MalformattedElementException("CS Info without Inter PoP Ids list");
	}

}
