package edu.upc.gco.slcnt.rest.client.cp.cpsr.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ProblemDetails {

	@JsonProperty("type")
	private String type;
	@JsonProperty("title")
	private String title;
	@JsonProperty("status")
	private int status;
	@JsonProperty("detail")
	private String detail;
	@JsonProperty("instance")
	private String instance;
	@JsonProperty("cause")
	private String cause;
	@JsonProperty("invalidParams")
	private List<InvalidParam> invalidParams;
	
	public ProblemDetails (String type, String title, int status, String detail, String instance, String cause)
	{
		this.type = type;
		this.title = title;
		this.status = status;
		this.detail = detail;
		this.instance = instance;
		this.cause = cause;
		this.invalidParams = new ArrayList<InvalidParam>();
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public String getInstance() {
		return instance;
	}

	public void setInstance(String instance) {
		this.instance = instance;
	}

	public String getCause() {
		return cause;
	}

	public void setCause(String cause) {
		this.cause = cause;
	}

	public List<InvalidParam> getInvalidParams() {
		return invalidParams;
	}

	public void setInvalidParams(List<InvalidParam> invalidParams) {
		this.invalidParams = invalidParams;
	}
}
