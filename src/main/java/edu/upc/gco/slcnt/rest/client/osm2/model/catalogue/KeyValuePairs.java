package edu.upc.gco.slcnt.rest.client.osm2.model.catalogue;

import java.util.List;

public class KeyValuePairs {

	private List<KeyValuePairElement> keyValuePairs;

	public List<KeyValuePairElement> getKeyValuePairs() {
		return keyValuePairs;
	}

	public void setKeyValuePairs(List<KeyValuePairElement> keyValuePairs) {
		this.keyValuePairs = keyValuePairs;
	}
}
