package edu.upc.gco.slcnt.rest.client.osdncontroller.interfaces.sbi.elements;

import com.fasterxml.jackson.annotation.JsonProperty;

public class EndPoint {

	@JsonProperty("client_ip_network")
	private String clientIpNetwork;
	@JsonProperty("border_ip_network")
	private String borderIpNetwork;
	@JsonProperty("border_mac_address")
	private String borderMacAddress;
	@JsonProperty("in_port")
	private String inPort;
	@JsonProperty("out_port")
	private String outPort;
	
	public EndPoint(String clientIpNetwork, String borderIpNetwork, String borderMacAddress, String inPort, String outPort) {

		this.clientIpNetwork = clientIpNetwork;
		this.borderIpNetwork = borderIpNetwork;
		this.borderMacAddress = borderMacAddress;
		this.inPort = inPort;
		this.outPort = outPort;
	}

	public String getClientIpNetwork() {
		return clientIpNetwork;
	}

	public void setClientIpNetwork(String clientIpNetwork) {
		this.clientIpNetwork = clientIpNetwork;
	}

	public String getBorderIpNetwork() {
		return borderIpNetwork;
	}

	public void setBorderIpNetwork(String borderIpNetwork) {
		this.borderIpNetwork = borderIpNetwork;
	}

	public String getBorderMacAddress() {
		return borderMacAddress;
	}

	public void setBorderMacAddress(String borderMacAddress) {
		this.borderMacAddress = borderMacAddress;
	}

	public String getInPort() {
		return inPort;
	}

	public void setInPort(String inPort) {
		this.inPort = inPort;
	}

	public String getOutPort() {
		return outPort;
	}

	public void setOutPort(String outPort) {
		this.outPort = outPort;
	}
	
	public String toString() {
		
		String s = "Client IP network: "+clientIpNetwork+"; Border IP network: "+borderIpNetwork+"; Border MAC address: "+borderMacAddress+"; In port: "+inPort+" Out port: "+outPort;
		
		return s;
	}
}