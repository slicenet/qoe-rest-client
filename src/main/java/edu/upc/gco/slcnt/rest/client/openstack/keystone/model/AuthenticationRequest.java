package edu.upc.gco.slcnt.rest.client.openstack.keystone.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AuthenticationRequest {

	@JsonProperty("auth")
	private Authentication auth = null;
	
	public AuthenticationRequest (Authentication auth)
	{
		this.auth = auth;
	}

	public Authentication getAuth() {
		return auth;
	}

	public void setAuth(Authentication auth) {
		this.auth = auth;
	}
}
