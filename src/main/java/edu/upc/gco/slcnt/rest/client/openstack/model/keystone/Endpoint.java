package edu.upc.gco.slcnt.rest.client.openstack.model.keystone;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Endpoint {

	@JsonProperty("url")
	private String url;
	
	@JsonProperty("interface")
	private String iface;
	
	@JsonProperty("region")
	private String region;
	
	@JsonProperty("region_id")
	private String regionId;
	
	@JsonProperty("id")
	private String id;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getIface() {
		return iface;
	}

	public void setIface(String iface) {
		this.iface = iface;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getRegionId() {
		return regionId;
	}

	public void setRegionId(String regionId) {
		this.regionId = regionId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
