package edu.upc.gco.slcnt.rest.client.osm2.model.catalogue;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Storage {

	@JsonProperty("folder")
	private String folder;				// == NSD Id
	@JsonProperty("path")
	private String path;
	@JsonProperty("descriptor")
	private String descriptor;
	@JsonProperty("pkg-dir")
	private String pkgDir;
	@JsonProperty("zipfile")
	private String zipFile;
	@JsonProperty("fs")
	private String fs;
	
	public String getFolder() {
		return folder;
	}
	public void setFolder(String folder) {
		this.folder = folder;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getDescriptor() {
		return descriptor;
	}
	public void setDescriptor(String descriptor) {
		this.descriptor = descriptor;
	}
	public String getPkgDir() {
		return pkgDir;
	}
	public void setPkgDir(String pkgDir) {
		this.pkgDir = pkgDir;
	}
	public String getZipFile() {
		return zipFile;
	}
	public void setZipFile(String zipFile) {
		this.zipFile = zipFile;
	}
	public String getFs() {
		return fs;
	}
	public void setFs(String fs) {
		this.fs = fs;
	}
}
