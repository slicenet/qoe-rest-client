package edu.upc.gco.slcnt.rest.client.cp.cpsr.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class IpEndPoint {

	@JsonProperty("ipv4Address")
	private String ipv4Address;
	@JsonProperty("ipv6Address")
	private String ipv6Address;
	@JsonProperty("ipv6Prefix")
	private String ipv6Prefix;
	@JsonProperty("transport")
	private String transport;
	@JsonProperty("port")
	private int port;
	
	public IpEndPoint ()
	{
		
	}
	
	public IpEndPoint (String ipv4Address, String ipv6Address, String ipv6Prefix, String transport, int port)
	{
		this.ipv4Address = ipv4Address;
		this.ipv6Address = ipv6Address;
		this.ipv6Prefix = ipv6Prefix;
		this.transport = transport;
		this.port = port;
	}

	public String getIpv4Address() {
		return ipv4Address;
	}

	public void setIpv4Address(String ipv4Address) {
		this.ipv4Address = ipv4Address;
	}

	public String getIpv6Address() {
		return ipv6Address;
	}

	public void setIpv6Address(String ipv6Address) {
		this.ipv6Address = ipv6Address;
	}

	public String getIpv6Prefix() {
		return ipv6Prefix;
	}

	public void setIpv6Prefix(String ipv6Prefix) {
		this.ipv6Prefix = ipv6Prefix;
	}

	public String getTransport() {
		return transport;
	}

	public void setTransport(String transport) {
		this.transport = transport;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}
}
