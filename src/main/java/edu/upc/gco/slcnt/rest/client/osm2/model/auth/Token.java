package edu.upc.gco.slcnt.rest.client.osm2.model.auth;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Token {

	@JsonProperty("id")
	private String id;
	@JsonProperty("admin")
	private boolean admin;
	@JsonProperty("username")
	private String username;
	@JsonProperty("remote_host")
	private String remoteHost;
	@JsonProperty("user_id")
	private String userId;
	@JsonProperty("project_id")
	private String projectId;
	@JsonProperty("issued_at")
	private float issuedAt;
	@JsonProperty("expires")
	private float expires;
	@JsonProperty("roles")
	private List<Role> roles;
	@JsonProperty("remote_port")
	private int remotePort;
	@JsonProperty("project_name")
	private String projectName;
	@JsonProperty("_id")
	private String tokenId;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public boolean isAdmin() {
		return admin;
	}
	public void setAdmin(boolean admin) {
		this.admin = admin;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getRemoteHost() {
		return remoteHost;
	}
	public void setRemoteHost(String remoteHost) {
		this.remoteHost = remoteHost;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getProjectId() {
		return projectId;
	}
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}
	public float getIssuedAt() {
		return issuedAt;
	}
	public void setIssuedAt(float issuedAt) {
		this.issuedAt = issuedAt;
	}
	public float getExpires() {
		return expires;
	}
	public void setExpires(float expires) {
		this.expires = expires;
	}
	public List<Role> getRoles() {
		return roles;
	}
	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}
	public int getRemotePort() {
		return remotePort;
	}
	public void setRemotePort(int remotePort) {
		this.remotePort = remotePort;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getTokenId() {
		return tokenId;
	}
	public void setTokenId(String tokenId) {
		this.tokenId = tokenId;
	}
	
	public String toString ()
	{
		return "Token = " + this.id;
	}
}
