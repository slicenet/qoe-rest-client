package edu.upc.gco.slcnt.rest.client.cp.cpsr.model;

import java.util.ArrayList;
import java.util.List;

public class DefaultNotificationSubscription extends ArrayList<String> {

	@Override
	public boolean equals (java.lang.Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		return super.equals(o);
	}
}
