package edu.upc.gco.slcnt.rest.client.openstack.keystone.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class User {

	@JsonProperty("name")
	private String name = null;
	
	@JsonProperty("id")
	private String id = null;
	
	@JsonProperty("domain")
	private Domain domain = null;
	
	@JsonProperty("password")
	private String password = null;
	
	public User (String id, String password)
	{
		this.id = id;
		this.password = password;
	}
	
	public User (String name, Domain domain, String password)
	{
		this.name = name;
		this.domain = domain;
		this.password = password;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Domain getDomain() {
		return domain;
	}

	public void setDomain(Domain domain) {
		this.domain = domain;
	}
}
