package edu.upc.gco.slcnt.rest.client.osm2.model.catalogue;

import com.fasterxml.jackson.annotation.JsonProperty;

public class InterfaceInfo {

	@JsonProperty("external-connection-point-ref")
	private String externalCPRef;
	@JsonProperty("virtual-interface")
	private VirtualInterface vIf;
	@JsonProperty("name")
	private String name;
	
	public String getExternalCPRef() {
		return externalCPRef;
	}
	public void setExternalCPRef(String externalCPRef) {
		this.externalCPRef = externalCPRef;
	}
	public VirtualInterface getvIf() {
		return vIf;
	}
	public void setvIf(VirtualInterface vIf) {
		this.vIf = vIf;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
