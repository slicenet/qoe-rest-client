package edu.upc.gco.slcnt.rest.client.openstack.model.keystone;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class Project {

	@JsonProperty("id")
	private String id = null;
	
	@JsonProperty("domain")
	private Domain domain = null;
	
	@JsonProperty("name")
	private String name = null;
	
	public Project (String id, Domain domain, String name)
	{
		this.id = id;
		this.domain = domain;
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Domain getDomain() {
		return domain;
	}

	public void setDomain(Domain domain) {
		this.domain = domain;
	}
}
