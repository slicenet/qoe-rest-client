package edu.upc.gco.slcnt.rest.client.osdncontroller.interfaces.sbi.elements;

public class Port {

	private String portId;
	
	public Port(String portId) {
		
		this.portId = portId;
	}

	public String getPortId() {
		return portId;
	}

	public void setPortId(String portId) {
		this.portId = portId;
	}
}