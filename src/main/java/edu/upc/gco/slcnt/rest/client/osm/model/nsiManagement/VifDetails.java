package edu.upc.gco.slcnt.rest.client.osm.model.nsiManagement;

import com.fasterxml.jackson.annotation.JsonProperty;

public class VifDetails {

	@JsonProperty("bridge_name")
	private String bridgeName;
	
	@JsonProperty("datapath_type")
	private String datapathType;
	
	@JsonProperty("ovs_hybrid_plug")
	private boolean ovsHybridPlug;
	
	@JsonProperty("port_filter")
	private boolean portFilter;

	public String getBridgeName() {
		return bridgeName;
	}

	public void setBridgeName(String bridgeName) {
		this.bridgeName = bridgeName;
	}

	public String getDatapathType() {
		return datapathType;
	}

	public void setDatapathType(String datapathType) {
		this.datapathType = datapathType;
	}

	public boolean isOvsHybridPlug() {
		return ovsHybridPlug;
	}

	public void setOvsHybridPlug(boolean ovsHybridPlug) {
		this.ovsHybridPlug = ovsHybridPlug;
	}

	public boolean isPortFilter() {
		return portFilter;
	}

	public void setPortFilter(boolean portFilter) {
		this.portFilter = portFilter;
	}
}
