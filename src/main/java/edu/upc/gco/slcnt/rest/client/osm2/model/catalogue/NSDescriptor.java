package edu.upc.gco.slcnt.rest.client.osm2.model.catalogue;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import edu.upc.gco.slcnt.rest.client.osm2.model.common.VnfDescriptorRefElement;

/**
 * NSD base information object.
 * 
 * @author fernando
 *
 */
public class NSDescriptor {

	@JsonProperty("constituent-vnfd")
	private List<VnfDescriptorRefElement> vnfdList;
	@JsonProperty("version")
	private String version;
	@JsonProperty("vld")
	private List<VLDescriptor> vldList;
	@JsonProperty("description")
	private String description;
	@JsonProperty("name")
	private String name;
	@JsonProperty("id")
	private String id;
	@JsonProperty("short-name")
	private String shortName;
	@JsonProperty("_id")
	private String nsdId;
	@JsonProperty("_admin")
	private AdminInfo adminInfo;
	@JsonProperty("vendor")
	private String vendor;
	
	public List<VnfDescriptorRefElement> getVnfdList() {
		return vnfdList;
	}
	public void setVnfdList(List<VnfDescriptorRefElement> vnfdList) {
		this.vnfdList = vnfdList;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public List<VLDescriptor> getVldList() {
		return vldList;
	}
	public void setVldList(List<VLDescriptor> vldList) {
		this.vldList = vldList;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getShortName() {
		return shortName;
	}
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}
	public String getNsdId() {
		return nsdId;
	}
	public void setNsdId(String nsdId) {
		this.nsdId = nsdId;
	}
	public AdminInfo getAdminInfo() {
		return adminInfo;
	}
	public void setAdminInfo(AdminInfo adminInfo) {
		this.adminInfo = adminInfo;
	}
	public String getVendor() {
		return vendor;
	}
	public void setVendor(String vendor) {
		this.vendor = vendor;
	}
}
