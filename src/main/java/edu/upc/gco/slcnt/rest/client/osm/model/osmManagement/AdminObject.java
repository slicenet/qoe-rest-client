package edu.upc.gco.slcnt.rest.client.osm.model.osmManagement;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;

public class AdminObject {

	@JsonProperty("deployed")
	private DeployedObject deployed;
	@JsonProperty("projects_write")
	private List<String> projectsWrite;	//OSM projectIds
	@JsonProperty("projects_read")
	private List<String> projectsRead;	//OSM projectIds
	@JsonProperty("operationalState")
	private String operationalState;
	@JsonProperty("modified")
	private float modified;
	@JsonProperty("detailed-status")
	private String detailedStatus;
	@JsonProperty("created")
	private float created;
	
	@Transient
    private Map<String, Object> otherProperties = new HashMap<String, Object>();
	
	public DeployedObject getDeployed() {
		return deployed;
	}
	public void setDeployed(DeployedObject deployed) {
		this.deployed = deployed;
	}
	public List<String> getProjectsWrite() {
		return projectsWrite;
	}
	public void setProjectsWrite(List<String> projectsWrite) {
		this.projectsWrite = projectsWrite;
	}
	public float getModified() {
		return modified;
	}
	public void setModified(float modified) {
		this.modified = modified;
	}
	public List<String> getProjectsRead() {
		return projectsRead;
	}
	public void setProjectsRead(List<String> projectsRead) {
		this.projectsRead = projectsRead;
	}
	public String getOperationalState() {
		return operationalState;
	}
	public void setOperationalState(String operationalState) {
		this.operationalState = operationalState;
	}
	public String getDetailedStatus() {
		return detailedStatus;
	}
	public void setDetailedStatus(String detailedStatus) {
		this.detailedStatus = detailedStatus;
	}
	public Map<String, Object> getOtherProperties() {
		return otherProperties;
	}
	public void setOtherProperties(Map<String, Object> otherProperties) {
		this.otherProperties = otherProperties;
	}
	public float getCreated() {
		return created;
	}
	public void setCreated(float created) {
		this.created = created;
	}
	
	@JsonAnyGetter
    public Map<String, Object> any() {
        return otherProperties;
    }

    @JsonAnySetter
    public void set(String name, Object value) {
        otherProperties.put(name, value);
    }
    
	@Override
	public String toString() {
		return "AdminObject [deployed=" + deployed + ", projectsWrite=" + projectsWrite + ", projectsRead="
				+ projectsRead + ", operationalState=" + operationalState + ", modified=" + modified
				+ ", detailedStatus=" + detailedStatus + ", created=" + created + ", otherProperties=" + otherProperties
				+ "]";
	}
}
