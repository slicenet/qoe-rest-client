package edu.upc.gco.slcnt.rest.client.osm.model.osmManagement;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;

public class VimInfoObject {

	@JsonProperty("name")
	private String name;
	@JsonProperty("vim_url")
	private String url;
	@JsonProperty("vim_user")
	private String user;
	@JsonProperty("config")
	private VimConfig config;
	@JsonProperty("vim_password")
	private String password;
	@JsonProperty("vim_tenant_name")
	private String tenantName;
	@JsonProperty("_id")
	private String id;
	@JsonProperty("vim_type")
	private String type;
	@JsonProperty("_admin")
	private AdminObject admin;
	@JsonProperty("schema_version")
	private String schemaVersion;
	@JsonProperty("description")
	private String description;
	
	@Transient
    private Map<String, Object> otherProperties = new HashMap<String, Object>();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public VimConfig getConfig() {
		return config;
	}

	public void setConfig(VimConfig config) {
		this.config = config;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getTenantName() {
		return tenantName;
	}

	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public AdminObject getAdmin() {
		return admin;
	}

	public void setAdmin(AdminObject admin) {
		this.admin = admin;
	}

	public String getSchemaVersion() {
		return schemaVersion;
	}

	public void setSchemaVersion(String schemaVersion) {
		this.schemaVersion = schemaVersion;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	@JsonAnyGetter
    public Map<String, Object> any() {
        return otherProperties;
    }

    @JsonAnySetter
    public void set(String name, Object value) {
        otherProperties.put(name, value);
    }

	@Override
	public String toString() {
		return "VimInfoObject [name=" + name + ", url=" + url + ", user=" + user + ", config=" + config + ", password="
				+ password + ", tenantName=" + tenantName + ", id=" + id + ", type=" + type + ", admin=" + admin
				+ ", schemaVersion=" + schemaVersion + ", description=" + description + ", otherProperties="
				+ otherProperties + "]";
	}
}
