package edu.upc.gco.slcnt.rest.client.pandp;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import edu.upc.gco.slcnt.rest.client.Utils;

public class PandPApi {

	private Log logger = LogFactory.getLog(PandPApi.class);
	private String PandPUri;
	
	public PandPApi (String PandPUri)
	{
		this.PandPUri = PandPUri;
	}
	
	public void registerPlugin (String configJson)
    {
    	
    	RestTemplate rt = new RestTemplate(Utils.getClientHttpRequestFactory());
    	HttpHeaders headers = new HttpHeaders();
    	headers.setContentType(MediaType.APPLICATION_JSON);
    	HttpEntity<String> qoeJson = null;
		
    	qoeJson = new HttpEntity<String>(configJson, headers);
		
    	try
    	{
    		rt.postForObject(this.PandPUri, qoeJson, String.class);
    	}
    	catch (RestClientException rce)
    	{
    		logger.error(rce.getMessage());
    	}
    }
}
