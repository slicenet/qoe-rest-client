package edu.upc.gco.slcnt.rest.client.osdncontroller.interfaces.sbi.elements;

import java.util.List;

public class OXCconfiguration {

	private String oxc;
	private String cmd;
	private List<EndPoint> end_points;
	private Params params;
	
	public OXCconfiguration(String oxc, String cmd, List<EndPoint> end_points, Params params) {

		this.oxc = oxc;
		this.cmd = cmd;
		this.end_points = end_points;
		this.params = params;
	}

	public String getOxc() {
		return oxc;
	}

	public void setOxc(String oxc) {
		this.oxc = oxc;
	}

	public String getCmd() {
		return cmd;
	}

	public void setCmd(String cmd) {
		this.cmd = cmd;
	}

	public List<EndPoint> getEnd_points() {
		return end_points;
	}

	public void setEnd_points(List<EndPoint> end_points) {
		this.end_points = end_points;
	}

	public Params getParams() {
		return params;
	}

	public void setParams(Params params) {
		this.params = params;
	}
}