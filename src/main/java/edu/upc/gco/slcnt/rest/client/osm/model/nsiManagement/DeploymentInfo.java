package edu.upc.gco.slcnt.rest.client.osm.model.nsiManagement;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;

public class DeploymentInfo {

	@JsonProperty("RO")
	private RO ro;
	@JsonProperty("VCA")
	private List vcaList;
	
	@Transient
    private Map<String, Object> otherProperties = new HashMap<String, Object>();
	
	public RO getRo() {
		return ro;
	}
	public void setRo(RO ro) {
		this.ro = ro;
	}
	public List getVcaList() {
		return vcaList;
	}
	public void setVcaList(List vcaList) {
		this.vcaList = vcaList;
	}
	
	@JsonAnyGetter
    public Map<String, Object> any() {
        return otherProperties;
    }

    @JsonAnySetter
    public void set(String name, Object value) {
        otherProperties.put(name, value);
    }
    
	@Override
	public String toString() {
		return "DeploymentInfo [ro=" + ro + ", vcaList=" + vcaList + ", otherProperties=" + otherProperties + "]";
	}
}
