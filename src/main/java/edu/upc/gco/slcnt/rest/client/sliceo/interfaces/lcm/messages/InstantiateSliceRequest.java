package edu.upc.gco.slcnt.rest.client.sliceo.interfaces.lcm.messages;

import java.util.HashMap;
import java.util.List;

import edu.upc.gco.slcnt.rest.client.common.interfaces.elements.EndPoint;
import it.nextworks.nfvmano.libs.ifa.common.InterfaceMessage;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.MalformattedElementException;

public class InstantiateSliceRequest implements InterfaceMessage {

	private String tenantId;
	private String sliceId;
	
	//private List<String> nsdIds;
	private List<EndPoint> endPoints;
	
	private HashMap<String,String> parameters;
	
	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	public String getSliceId() {
		return sliceId;
	}

	public void setSliceId(String sliceId) {
		this.sliceId = sliceId;
	}

	/*public List<String> getNsdIds() {
		return nsdIds;
	}

	public void setNsdIds(List<String> nsdIds) {
		this.nsdIds = nsdIds;
	}*/

	public List<EndPoint> getEndPoints() {
		return endPoints;
	}

	public void setEndPoints(List<EndPoint> endPoints) {
		this.endPoints = endPoints;
	}

	public HashMap<String, String> getParameters() {
		return parameters;
	}

	public void setParameters(HashMap<String, String> parameters) {
		this.parameters = parameters;
	}

	@Override
	public void isValid() throws MalformattedElementException {
		
		if (tenantId == null) throw new MalformattedElementException("Slice Instatiation request without Tenant ID");
		if (sliceId == null) throw new MalformattedElementException("Slice Instatiation request without Slice ID");
		//if (nsdIds == null) throw new MalformattedElementException("Slice Instatiation request without NSD list");
	}
}
