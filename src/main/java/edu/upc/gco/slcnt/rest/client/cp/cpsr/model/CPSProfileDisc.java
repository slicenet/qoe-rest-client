package edu.upc.gco.slcnt.rest.client.cp.cpsr.model;

import java.util.List;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CPSProfileDisc {
	
	@JsonProperty("cpsInstanceId")
	private UUID instanceId;	//CPS Instance Id
	@JsonProperty("cpsType")
	private CPSType type;
	@JsonProperty("slicenetId")
	private UUID sliceId;
	@JsonProperty("fqdn")
	private String fqdn;
	@JsonProperty("ipv4Addresses")
	private List<String> ipv4Addresses;
	@JsonProperty("ipv6Addresses")
	private List<String> ipv6Addresses;
	@JsonProperty("ipv6Prefixes")
	private List<String> ipv6Prefixes;
	@JsonProperty("capacity")
	private int capacity;
	@JsonProperty("load")
	private int load;
	@JsonProperty("priority")
	private int priority;
	@JsonProperty("cpsServices")
	private List<CPSServiceDisc> services;
	
	public UUID getInstanceId() {
		return instanceId;
	}
	public void setInstanceId(UUID nstanceId) {
		this.instanceId = nstanceId;
	}
	public CPSType getType() {
		return type;
	}
	public void setType(CPSType type) {
		this.type = type;
	}
	public UUID getSliceId() {
		return sliceId;
	}
	public void setSliceId(UUID sliceId) {
		this.sliceId = sliceId;
	}
	public String getFqdn() {
		return fqdn;
	}
	public void setFqdn(String fqdn) {
		this.fqdn = fqdn;
	}
	public List<String> getIpv4Addresses() {
		return ipv4Addresses;
	}
	public void setIpv4Addresses(List<String> ipv4Addresses) {
		this.ipv4Addresses = ipv4Addresses;
	}
	public List<String> getIpv6Addresses() {
		return ipv6Addresses;
	}
	public void setIpv6Addresses(List<String> ipv6Addresses) {
		this.ipv6Addresses = ipv6Addresses;
	}
	public List<String> getIpv6Prefixes() {
		return ipv6Prefixes;
	}
	public void setIpv6Prefixes(List<String> ipv6Prefixes) {
		this.ipv6Prefixes = ipv6Prefixes;
	}
	public int getCapacity() {
		return capacity;
	}
	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}
	public int getLoad() {
		return load;
	}
	public void setLoad(int load) {
		this.load = load;
	}
	public int getPriority() {
		return priority;
	}
	public void setPriority(int priority) {
		this.priority = priority;
	}
	public List<CPSServiceDisc> getServices() {
		return services;
	}
	public void setServices(List<CPSServiceDisc> services) {
		this.services = services;
	}
}
