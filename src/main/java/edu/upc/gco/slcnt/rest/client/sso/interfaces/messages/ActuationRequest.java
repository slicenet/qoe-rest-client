package edu.upc.gco.slcnt.rest.client.sso.interfaces.messages;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class ActuationRequest {

	private String nsiId;
	private String actuationName;
	private String description;
	private Map<String,String> parameters;
	private String notificationEndpoint;
	
	public ActuationRequest()
	{
		
	}
	
	public ActuationRequest (String nsiId, String actuationName, String description, Map<String,String> parameters, String notificationEndpoint)
	{
		this.nsiId = nsiId;
		this.actuationName = actuationName;
		this.description = description;
		this.parameters = parameters;
		this.notificationEndpoint = notificationEndpoint;
	}
	
	public String getNsiId() {
		return nsiId;
	}

	public String getActuationName() {
		return actuationName;
	}

	public String getDescription() {
		return description;
	}

	public Map<String, String> getParameters() {
		return parameters;
	}

	public String getNotificationEndpoint() {
		return notificationEndpoint;
	}

	@Override
	public String toString()
	{
		String str = "{\n";
		str += "\t\"nsiId\": \"" + nsiId +"\",\n";
		str += "\t\"actuationName\": \"" + actuationName + "\",\n";
		str += "\t\"description\": \"" + description + "\",\n";
		str += "\t\"parameters\": {\n";
		int i = 0;
		for (Map.Entry<String, String> entry : parameters.entrySet())
		{
			str += "\t\t\"" + entry.getKey() + "\": \"" + entry.getValue() + "\"" + ((++i)==parameters.size()?"\n":",\n");
		}
		
		if (notificationEndpoint != null)
		{
			str += "\t},\n";
			str += "\t\"notificationEndpoint\": \"" + notificationEndpoint + "\"\n";
		}
		else
			str += "\t}\n";
		
		str += "}\n";
		
		return str;
	}
}
