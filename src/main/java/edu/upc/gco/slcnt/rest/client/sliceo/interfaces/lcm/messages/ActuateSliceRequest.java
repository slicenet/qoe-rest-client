package edu.upc.gco.slcnt.rest.client.sliceo.interfaces.lcm.messages;

import java.util.Map;

import it.nextworks.nfvmano.libs.ifa.common.InterfaceMessage;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.MalformattedElementException;

public class ActuateSliceRequest implements InterfaceMessage {

	//TODO: Add the DSP Tenant Id to avoid looking for all Tenants 
	private String sliceId;
	private String actuationName;
	private String description;
	private Map<String,String> parameters;
	private String notificationEndpoint;
	
	public ActuateSliceRequest ()
	{
		
	}
	
	public ActuateSliceRequest (String sliceId, String actuationName, String description, Map<String,String> parameters, String notificationEndpoint)
	{
		this.sliceId = sliceId;
		this.actuationName = actuationName;
		this.description = description;
		this.parameters = parameters;
		this.notificationEndpoint = notificationEndpoint;
	}
	
	public String getSliceId() {
		return sliceId;
	}

	public String getActuationName() {
		return actuationName;
	}

	public Map<String, String> getParameters() {
		return parameters;
	}

	public String getDescription() {
		return description;
	}

	public String getNotificationEndpoint() {
		return notificationEndpoint;
	}

	public void setSliceId(String sliceId) {
		this.sliceId = sliceId;
	}

	public void setActuationName(String actuationName) {
		this.actuationName = actuationName;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setParameters(Map<String, String> parameters) {
		this.parameters = parameters;
	}

	public void setNotificationEndpoint(String notificationEndpoint) {
		this.notificationEndpoint = notificationEndpoint;
	}

	@Override
	public void isValid() throws MalformattedElementException {
		if (sliceId == null) throw new MalformattedElementException("Actuate Slice request without Slice instance ID");
		if (actuationName == null) throw new MalformattedElementException("Actuate Slice request without Actuation Name");
	}

}
