package edu.upc.gco.slcnt.rest.client.osdncontroller.interfaces.nbi.messages;

import java.util.HashMap;

import it.nextworks.nfvmano.libs.ifa.common.InterfaceMessage;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.MalformattedElementException;

public class GetSdnIntentInfoResponse implements InterfaceMessage {

	private HashMap<String, String> intentInfo = new HashMap<String, String>();
	
	public GetSdnIntentInfoResponse() {
		
	}
	
	public GetSdnIntentInfoResponse(HashMap<String, String> intentInfo) {
		
		this.intentInfo = intentInfo;
	}
	
	public HashMap<String, String> getIntentInfo(){
		
		return intentInfo;
	}
	
	@Override
	public void isValid() throws MalformattedElementException
	{
		
	}
}