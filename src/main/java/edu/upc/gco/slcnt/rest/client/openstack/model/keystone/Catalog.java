package edu.upc.gco.slcnt.rest.client.openstack.model.keystone;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Catalog {

	@JsonProperty("endpoints")
	private List<Endpoint> endpoints;
	
	@JsonProperty("type")
	private String type;
	
	@JsonProperty("id")
	private String id;
	
	@JsonProperty("name")
	private String name;

	public List<Endpoint> getEndpoints() {
		return endpoints;
	}

	public void setEndpoints(List<Endpoint> endpoints) {
		this.endpoints = endpoints;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
