package edu.upc.gco.slcnt.rest.client.nmro.interfaces.cslcm.messages;

import java.util.ArrayList;
import java.util.List;

import edu.upc.gco.slcnt.rest.client.nmro.interfaces.cslcm.elements.CsInfo;
import it.nextworks.nfvmano.libs.ifa.common.InterfaceMessage;
import it.nextworks.nfvmano.libs.ifa.common.enums.ResponseCode;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.MalformattedElementException;

public class QueryCsResponse implements InterfaceMessage {

	List<CsInfo> queryCsResult = new ArrayList<>();
	
	public QueryCsResponse() { }
	
	public QueryCsResponse (ResponseCode responseCode, List<CsInfo> queryCsResult)
	{
		if (queryCsResult != null)
			this.queryCsResult = queryCsResult;
	}


	public List<CsInfo> getQueryCsResult()
	{
		return queryCsResult;
	}
	
	@Override
	public void isValid() throws MalformattedElementException {
		if (queryCsResult != null)
		{
			for (CsInfo info : queryCsResult) info.isValid();
		}
	}

}
