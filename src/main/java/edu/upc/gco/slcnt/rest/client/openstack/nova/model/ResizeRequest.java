package edu.upc.gco.slcnt.rest.client.openstack.nova.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResizeRequest {

	@JsonProperty("resize")
	private Resize resize = null;
	
	public ResizeRequest(Resize resize)
	{
		this.resize = resize;
	}

	public Resize getResize() {
		return resize;
	}

	public void setResize(Resize resize) {
		this.resize = resize;
	}
}
