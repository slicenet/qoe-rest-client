package edu.upc.gco.slcnt.rest.client.sso;

import org.apache.http.HttpHost;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.web.client.RestTemplate;

import edu.upc.gco.slcnt.rest.client.Utils;
import edu.upc.gco.slcnt.rest.client.sso.interfaces.messages.ActuationRequest;

public class ServiceOrchestratorClient {

	private static final Logger log = LoggerFactory.getLogger(ServiceOrchestratorClient.class);
	
	private String e2eNsiId;
	private RestTemplate restTemplate = new RestTemplate();
	private String protocol, ipAddress, port, user, password;
	
	private String soLifecycleServiceUrl;
	private String cookie;
	final private String e2eVSLcm = "/vs/basic/vslcm";
	final private String e2eNsi = "/e2ens";
	
	public ServiceOrchestratorClient (String e2eNsiId, /*String soLifecycleServiceUrl*/String protocol, String ipAddress, String port, String user, String password)
	{
		this.e2eNsiId = e2eNsiId;
		this.protocol = protocol;
		this.ipAddress = ipAddress;
		this.port = port;
		this.user = user;
		this.password = password;
		this.soLifecycleServiceUrl = /*soLifecycleServiceUrl*/protocol + "://" + ipAddress + ":" + port;
		
		//Not needed, I think, since the login is done before each actuation request to avoid cookie expiration
		//login(user,password);
	}
	
	private void login (String username, String password)
	{
		String url = soLifecycleServiceUrl + "/login?username=" + username + "&password=" + password;
		HttpHeaders header = new HttpHeaders();
		HttpEntity<Void> request = new HttpEntity(header);
		ResponseEntity<Void> httpResponse = restTemplate.exchange(url, HttpMethod.POST, request, Void.class);
		for (String cookieStr : httpResponse.getHeaders().get("Set-Cookie"))
		{
			log.info("Cookies = " + cookieStr);
		}
		cookie = httpResponse.getHeaders().get("Set-Cookie").get(0);
	}
	
	public void e2eNsiActuation (ActuationRequest request)
	{
		/*HttpHost host = new HttpHost(ipAddress, Integer.parseInt(port), protocol);
		ClientHttpRequestFactory requestFactory = Utils.getBasicAuthClientHttpRequestFactory(host);
		restTemplate = new RestTemplate(requestFactory);
		restTemplate.getInterceptors().add(new BasicAuthorizationInterceptor("admin","admin"));*/
		
		// Log in before each actuation to avoid 30 minute cookie expiration. TODO: Manage expiration
		login(user,password);
		
		log.debug("Building HTTP request to actuate over the E2E Network Slice Instance = " + e2eNsiId);
		HttpHeaders header = new HttpHeaders();
		header.add("Content-Type", "application/json");
		header.add("Cookie", cookie);
		HttpEntity<ActuationRequest> postEntity = new HttpEntity<ActuationRequest>(request, header);

		String url = soLifecycleServiceUrl + e2eVSLcm  + e2eNsi + "/" + e2eNsiId + "/actuate";
		log.info("Sending POST to " + url + " with JSON:\n" + /*request.toString()*/postEntity.getBody().toString());
		log.debug("Sending HTTP request to actuate over the E2E Network Slice Instance.");
		ResponseEntity<Void> httpResponse = restTemplate.exchange(url, HttpMethod.POST, postEntity, Void.class);
		log.info("Result = " + httpResponse.getStatusCodeValue());
	}
}
