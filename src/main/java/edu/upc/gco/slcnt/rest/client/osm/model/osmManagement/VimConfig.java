package edu.upc.gco.slcnt.rest.client.osm.model.osmManagement;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;

public class VimConfig {

	@JsonProperty("use_floating_ip")
	private boolean useFloatingIp;
	@JsonProperty("region_name")
	private String regionName;
	@JsonProperty("user_domain_name")
	private String userDomainName;
	@JsonProperty("security_groups")
	private String securityGroups;
	@JsonProperty("project_domain_name")
	private String domainName;
	@JsonProperty("keypair")
	private String keyPair;
	@JsonProperty("APIversion")
	private String apiVersion;
	@JsonProperty("insecure")
	private boolean insecure;
	@JsonProperty("use_internal_endpoint")
	private boolean useInternalEndpoint;
	@JsonProperty("vim_type")
	private String type;
	
	@Transient
    private Map<String, Object> otherProperties = new HashMap<String, Object>();

	public boolean isUseFloatingIp() {
		return useFloatingIp;
	}

	public void setUseFloatingIp(boolean useFloatingIp) {
		this.useFloatingIp = useFloatingIp;
	}

	public String getRegionName() {
		return regionName;
	}

	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}

	public String getUserDomainName() {
		return userDomainName;
	}

	public void setUserDomainName(String userDomainName) {
		this.userDomainName = userDomainName;
	}

	public String getSecurityGroups() {
		return securityGroups;
	}

	public void setSecurityGroups(String securityGroups) {
		this.securityGroups = securityGroups;
	}

	public String getDomainName() {
		return domainName;
	}

	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	public String getKeyPair() {
		return keyPair;
	}

	public void setKeyPair(String keyPair) {
		this.keyPair = keyPair;
	}
	
	public String getApiVersion() {
		return apiVersion;
	}

	public void setApiVersion(String apiVersion) {
		this.apiVersion = apiVersion;
	}

	public boolean isInsecure() {
		return insecure;
	}

	public void setInsecure(boolean insecure) {
		this.insecure = insecure;
	}

	public boolean isUseInternalEndpoint() {
		return useInternalEndpoint;
	}

	public void setUseInternalEndpoint(boolean useInternalEndpoint) {
		this.useInternalEndpoint = useInternalEndpoint;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Map<String, Object> getOtherProperties() {
		return otherProperties;
	}

	public void setOtherProperties(Map<String, Object> otherProperties) {
		this.otherProperties = otherProperties;
	}

	@JsonAnyGetter
    public Map<String, Object> any() {
        return otherProperties;
    }

    @JsonAnySetter
    public void set(String name, Object value) {
        otherProperties.put(name, value);
    }

	@Override
	public String toString() {
		return "VimConfig [useFloatingIp=" + useFloatingIp + ", regionName=" + regionName + ", userDomainName="
				+ userDomainName + ", securityGroups=" + securityGroups + ", domainName=" + domainName + ", keyPair="
				+ keyPair + ", apiVersion=" + apiVersion + ", insecure=" + insecure + ", useInternalEndpoint="
				+ useInternalEndpoint + ", type=" + type + ", otherProperties=" + otherProperties + "]";
	}
}
