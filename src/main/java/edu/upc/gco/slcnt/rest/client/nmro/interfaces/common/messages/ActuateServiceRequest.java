package edu.upc.gco.slcnt.rest.client.nmro.interfaces.common.messages;

import java.util.Map;

import it.nextworks.nfvmano.libs.ifa.common.InterfaceMessage;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.MalformattedElementException;

public class ActuateServiceRequest implements InterfaceMessage {

	private String serviceId;
	private String actuationName;
	private String description;
	private Map<String,String> parameters;
	private String notificationEndpoint;
	
	public ActuateServiceRequest ()
	{
		
	}
	
	public ActuateServiceRequest (String serviceId, String actuationName, String description, Map<String,String> parameters, String notificationEndpoint)
	{
		this.serviceId = serviceId;
		this.actuationName = actuationName;
		this.description = description;
		this.parameters = parameters;
		this.notificationEndpoint = notificationEndpoint;
	}
	
	public String getServiceId() {
		return serviceId;
	}

	public String getActuationName() {
		return actuationName;
	}

	public Map<String, String> getParameters() {
		return parameters;
	}

	public String getDescription() {
		return description;
	}

	public String getNotificationEndpoint() {
		return notificationEndpoint;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public void setActuationName(String actuationName) {
		this.actuationName = actuationName;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setParameters(Map<String, String> parameters) {
		this.parameters = parameters;
	}

	public void setNotificationEndpoint(String notificationEndpoint) {
		this.notificationEndpoint = notificationEndpoint;
	}

	@Override
	public void isValid() throws MalformattedElementException {
		if (serviceId == null) throw new MalformattedElementException("Actuate request without Service ID");
		if (actuationName == null) throw new MalformattedElementException("Actuate request without Actuation Name");
	}

}
