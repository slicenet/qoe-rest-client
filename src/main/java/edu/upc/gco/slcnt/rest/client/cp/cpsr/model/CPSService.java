package edu.upc.gco.slcnt.rest.client.cp.cpsr.model;

import java.util.List;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CPSService {

	@JsonProperty("serviceInstanceId")
	private UUID instanceId;										// Service Instance Identifier (uuid)
	@JsonProperty("serviceName")
	private String name;
	@JsonProperty("version")
	private String version;
	@JsonProperty("schema")
	private String schema;											// http, https, ...
	@JsonProperty("slicenetId")
	private UUID sliceId;											// == slicenetId from openapi spec (uuid)
	@JsonProperty("fqdn")
	private String fqdn;
	@JsonProperty("ipEndPoints")
	private List<IpEndPoint> endPoints;
	@JsonProperty("apiPrefix")
	private String apiPrefix;
	@JsonProperty("defaultNotificationSubscriptions")
	private List<DefaultNotificationSubscription> subscriptions;
	// Not Available for Discovery
	@JsonProperty("allowedCPSTypes")
	private List<CPSType> allowedTypes;
	@JsonProperty("allowedSlices")
	private List<UUID> allowedSlices;								// Slice uuids
	// End Not Available for Discovery
	@JsonProperty("capacity")
	private int capacity;
	@JsonProperty("load")
	private int load;
	
	public UUID getInstanceId() {
		return instanceId;
	}
	public void setInstanceId(UUID instanceId) {
		this.instanceId = instanceId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getSchema() {
		return schema;
	}
	public void setSchema(String schema) {
		this.schema = schema;
	}
	public UUID getSliceId() {
		return sliceId;
	}
	public void setSliceId(UUID sliceId) {
		this.sliceId = sliceId;
	}
	public String getFqdn() {
		return fqdn;
	}
	public void setFqdn(String fqdn) {
		this.fqdn = fqdn;
	}
	public List<IpEndPoint> getEndPoints() {
		return endPoints;
	}
	public void setEndPoints(List<IpEndPoint> endPoints) {
		this.endPoints = endPoints;
	}
	public String getApiPrefix() {
		return apiPrefix;
	}
	public void setApiPrefix(String apiPrefix) {
		this.apiPrefix = apiPrefix;
	}
	public List<DefaultNotificationSubscription> getSubscriptions() {
		return subscriptions;
	}
	public void setSubscriptions(List<DefaultNotificationSubscription> subscriptions) {
		this.subscriptions = subscriptions;
	}
	public List<CPSType> getAllowedTypes() {
		return allowedTypes;
	}
	public void setAllowedTypes(List<CPSType> allowedTypes) {
		this.allowedTypes = allowedTypes;
	}
	public List<UUID> getAllowedSlices() {
		return allowedSlices;
	}
	public void setAllowedSlices(List<UUID> allowedSlices) {
		this.allowedSlices = allowedSlices;
	}
	public int getCapacity() {
		return capacity;
	}
	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}
	public int getLoad() {
		return load;
	}
	public void setLoad(int load) {
		this.load = load;
	}
}
