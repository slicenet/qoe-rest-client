package edu.upc.gco.slcnt.rest.client.openstack.model.keystone;

import java.time.LocalDateTime;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Token {

	@JsonIgnore
	private String tokenStr;
	
	@JsonProperty("is_domain")
	private boolean isDomain;
	
	@JsonProperty("methods")
	private List<String> methods;
	
	@JsonProperty("roles")
	private List<Role> roles;
	
	@JsonProperty("is_admin_project")
	private boolean isAdminProject;
	
	@JsonProperty("catalog")
	private Catalog catalog;
	
	@JsonProperty("expires_at")
	private LocalDateTime expiresAt;
	
	@JsonProperty("user")
	private User user;
	
	@JsonProperty("audit_ids")
	private List<String> auditIds;
	
	@JsonProperty("issued_at")
	private LocalDateTime issuedAt;

	public String getTokenStr() {
		return tokenStr;
	}

	public void setTokenStr(String tokenStr) {
		this.tokenStr = tokenStr;
	}

	public boolean isDomain() {
		return isDomain;
	}

	public void setDomain(boolean isDomain) {
		this.isDomain = isDomain;
	}

	public List<String> getMethods() {
		return methods;
	}

	public void setMethods(List<String> methods) {
		this.methods = methods;
	}

	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	public boolean isAdminProject() {
		return isAdminProject;
	}

	public void setAdminProject(boolean isAdminProject) {
		this.isAdminProject = isAdminProject;
	}

	public Catalog getCatalog() {
		return catalog;
	}

	public void setCatalog(Catalog catalog) {
		this.catalog = catalog;
	}

	public LocalDateTime getExpiresAt() {
		return expiresAt;
	}

	public void setExpiresAt(LocalDateTime expiresAt) {
		this.expiresAt = expiresAt;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<String> getAuditIds() {
		return auditIds;
	}

	public void setAuditIds(List<String> auditIds) {
		this.auditIds = auditIds;
	}

	public LocalDateTime getIssuedAt() {
		return issuedAt;
	}

	public void setIssuedAt(LocalDateTime issuedAt) {
		this.issuedAt = issuedAt;
	}
}
