package edu.upc.gco.slcnt.rest.client.osm2.model.catalogue;

import com.fasterxml.jackson.annotation.JsonProperty;

public class VMFlavor {

	@JsonProperty("vcpu-count")
	private int vCpuCount;
	@JsonProperty("memory-mb")
	private int memoryMB;
	@JsonProperty("storage-gb")
	private int storageGB;
	
	public int getvCpuCount() {
		return vCpuCount;
	}
	public void setvCpuCount(int vCpuCount) {
		this.vCpuCount = vCpuCount;
	}
	public int getMemoryMB() {
		return memoryMB;
	}
	public void setMemoryMB(int memoryMB) {
		this.memoryMB = memoryMB;
	}
	public int getStorageGB() {
		return storageGB;
	}
	public void setStorageGB(int storageGB) {
		this.storageGB = storageGB;
	}
}
