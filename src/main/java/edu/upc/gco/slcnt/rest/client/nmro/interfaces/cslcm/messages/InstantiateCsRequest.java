package edu.upc.gco.slcnt.rest.client.nmro.interfaces.cslcm.messages;

import java.util.HashMap;
import java.util.List;

import edu.upc.gco.slcnt.rest.client.common.interfaces.elements.EndPoint;
import it.nextworks.nfvmano.libs.ifa.common.InterfaceMessage;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.MalformattedElementException;

public class InstantiateCsRequest implements InterfaceMessage {

	private String csInstanceId;
	private List<EndPoint> endPoints;
	
	private HashMap<String,String> parameters;
	
	public InstantiateCsRequest()
	{
		
	}
	
	public InstantiateCsRequest (String csInstanceId, List<EndPoint> endPoints)
	{
		this.csInstanceId = csInstanceId;
		this.endPoints = endPoints;
	}
	
	public InstantiateCsRequest (String csInstanceId, List<EndPoint> endPoints, HashMap<String,String> parameters)
	{
		this.csInstanceId = csInstanceId;
		this.endPoints = endPoints;
		this.parameters = parameters;
	}
	
	public String getCsInstanceId() {
		return csInstanceId;
	}

	public List<EndPoint> getEndPoints() {
		return endPoints;
	}

	public HashMap<String, String> getParameters() {
		return parameters;
	}

	@Override
	public void isValid() throws MalformattedElementException {
		if (csInstanceId == null)
			throw new MalformattedElementException("Instantiate CS request without CS instance ID");
		if (endPoints == null)
			throw new MalformattedElementException("Instantiate CS request without EndPoint List");
	}

}
