package edu.upc.gco.slcnt.rest.client.osm.model.osmManagement;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ProjectObject {

	@JsonProperty("_id")
	private String id;
	
	@JsonProperty("_admin")
	private ProjectAdminInfo adminInfo;
	
	@JsonProperty("name")
	private String name;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public ProjectAdminInfo getAdminInfo() {
		return adminInfo;
	}

	public void setAdminInfo(ProjectAdminInfo adminInfo) {
		this.adminInfo = adminInfo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "ProjectObject [id=" + id + ", adminInfo=" + adminInfo + ", name=" + name + "]";
	}
}
