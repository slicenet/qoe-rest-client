package edu.upc.gco.slcnt.influxdb.client.datalake.model;

import java.time.Instant;

import org.influxdb.annotation.Column;
import org.influxdb.annotation.Measurement;

@Measurement(name = "event")
public class NNEventPoint {

	@Column(name = "time")
	private Instant time;
	
	@Column(name = "tenantid")
	private String tenantId;
	
	@Column(name = "vnfid")
	private String vnfId;
	
	@Column(name="type")
	private String type;
	
	@Column(name="flavor")
	private String flavor;

	public Instant getTime() {
		return time;
	}

	public void setTime(Instant time) {
		this.time = time;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	public String getVnfId() {
		return vnfId;
	}

	public void setVnfId(String vnfId) {
		this.vnfId = vnfId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getFlavor() {
		return flavor;
	}

	public void setFlavor(String flavor) {
		this.flavor = flavor;
	}
	
	@Override
	public String toString()
	{
		String str = "Time = " + this.time + " TenantId = " + this.tenantId + " VNFId = " + this.vnfId + " Type = " + this.type + " Flavor = " + this.getFlavor();
		
		return str;
	}
}
