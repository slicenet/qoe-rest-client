package edu.upc.gco.slcnt.influxdb.client.datalake;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBFactory;
import org.influxdb.InfluxDBMapperException;
import org.influxdb.dto.Point;
import org.influxdb.dto.Point.Builder;
import org.influxdb.dto.Query;
import org.influxdb.dto.QueryResult;
import org.influxdb.dto.QueryResult.Series;
import org.influxdb.impl.InfluxDBResultMapper;

import edu.upc.gco.slcnt.influxdb.client.datalake.model.NNEventPoint;

public class DataLakeApi {

	private Log logger = LogFactory.getLog(DataLakeApi.class);
	private InfluxDB dbcon;
	private QueryResult lastResult;
	
	private String protocol;
	private String ipAddress;
	private String port;
	
	public DataLakeApi()
	{
		
	}
	
	public DataLakeApi (String protocol, String ipAddress, String port)
	{
		this.protocol = protocol;
		this.ipAddress = ipAddress;
		this.port = port;
	}
	
	public void open ()
	{
		this.dbcon = InfluxDBFactory.connect(protocol + "://" + ipAddress + ":" + port);
	}
	
	public void open (String dbUrl)
	{
		this.dbcon = InfluxDBFactory.connect(dbUrl);
	}
	
	public void close()
	{
		this.dbcon.close();
	}
	
	public void getEvent (String dbName)
	{
		Query query = new Query ("select * from \"event\" where \"tenantid\"='tenantA'", dbName);
		QueryResult result = this.dbcon.query(query);
		InfluxDBResultMapper mapper = new InfluxDBResultMapper();
		List<NNEventPoint> nnpList = mapper.toPOJO(result, NNEventPoint.class);
		for (NNEventPoint nnp : nnpList)
		{
			logger.info(nnp.toString());
		}
	}
	
	private void processColumns (QueryResult result)
	{
		for (QueryResult.Result r : result.getResults())
		{
			for (Series s : r.getSeries())
			{
				logger.info("Series = " + s.getName());
				for (String c : s.getColumns())
					logger.info("Column = " + c);
				for (List<Object> v : s.getValues())
				{
					for ( Object o : v)
					{
						if (o != null)
							logger.info("Values = " + o.toString());
						else
							logger.info("Values = NULL");
					}
				}
			}
		}
	}
	
	private <T> void debugResult (List<T> list)
	{
		for (T tObj : list)
			logger.info(tObj.toString());
	}
	
	public <T> T getLastEvent (String dbName, String queryStr, Class<T> clazz)
	{
		// "group by" groups by series, so the limit 1 does not work if each entry is seen as different series (e.g. Fault Management)
		//logger.info("Query to " + dbName + " = " + queryStr + " group by * order by desc limit 1");
		//Query query = new Query (queryStr + " group by * order by desc limit 1", dbName);
		
		//logger.info("Query to " + dbName + " = " + queryStr + " order by desc limit 1");
		Query query = new Query (queryStr + " order by desc limit 1", dbName);
		QueryResult result = this.dbcon.query(query);
		InfluxDBResultMapper mapper = new InfluxDBResultMapper();
		
		lastResult = result;
		//processColumns (result);
		
		List<T> tList = null;
		try
		{
			tList = mapper.toPOJO(result, clazz);
		}
		catch (InfluxDBMapperException idbme)
		{
			logger.warn("Problem mapping the InfluxDB query result. Msg = " + idbme.getMessage());
			return null;
		}
		//debugResult(tList);
		if (tList.size() == 0)
			return null;
		T lastObj = tList.get(0);			// If Query is Desc, the last record is in postion 0
		//logger.info(lastObj.toString());
		
		return lastObj;
	}
	
	public void setPoint (String dbName, String measurement, Map<String,String> tags, Map<String,Object> fields)
	{
		Builder pb = Point.measurement(measurement);
		if (tags != null)
			pb.tag(tags);
		for (Entry<String, Object> e : fields.entrySet())
			logger.info("Entry: " + e.getKey() + " / " + e.getValue());
		pb.fields(fields);
		
		Point point = pb.build();
		
		this.dbcon.enableBatch(1, 200, TimeUnit.MILLISECONDS);
		//this.dbcon.setRetentionPolicy("defaultPolicy");
		this.dbcon.setDatabase(dbName);
		this.dbcon.write(point);
		//this.dbcon.flush();
		this.dbcon.disableBatch();
		this.dbcon.close();
	}

	public QueryResult getLastResult() {
		return lastResult;
	}
}
