package edu.upc.gco.slcnt.websocket.client.pf.spring;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.simp.stomp.StompSessionHandler;
import org.springframework.web.socket.client.WebSocketClient;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;

public class PFApi {

	private Log logger = LogFactory.getLog(PFApi.class);
	//WebSocketStompClient wsClient;
	WebSocketClient wsClient;
	
	public void connect ()
	{
		/*WebSocketClient client = new StandardWebSocketClient();
		this.wsClient = new WebSocketStompClient(client);
		
		this.wsClient.setMessageConverter(new MappingJackson2MessageConverter());
		
		StompSessionHandler sHandler = new PFStompSessionHandler();
		logger.info("Connecting to WebSocket");
		this.wsClient.connect("wss://172.26.37.15:2223/pdp/notifications", sHandler);*/
		
		this.wsClient = new StandardWebSocketClient();
	}
}
