package edu.upc.gco.slcnt.websocket.client.pf.javax;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import javax.net.ssl.SSLContext;
import javax.websocket.ClientEndpoint;
import javax.websocket.ClientEndpointConfig;
import javax.websocket.CloseReason;
import javax.websocket.ContainerProvider;
import javax.websocket.DeploymentException;
import javax.websocket.Endpoint;
import javax.websocket.EndpointConfig;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.WebSocketContainer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

//@ClientEndpoint
public class PFApi {

	private Log logger = LogFactory.getLog(PFApi.class);
	private PFClient wsClient = new PFClient();
	
	public void connect (URI endpointUri)
	{
		WebSocketContainer container = ContainerProvider.getWebSocketContainer();
		try {
			container.connectToServer(this, endpointUri);
		} catch (DeploymentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private ClientEndpointConfig createClientConfig (SSLContext context)
	{
		ClientEndpointConfig.Builder builder = ClientEndpointConfig.Builder.create();
		ClientEndpointConfig config = builder.decoders(new ArrayList()).encoders(new ArrayList()).preferredSubprotocols(new ArrayList()).build();
	    SSLContext sslContext = context;//SSLContexts.custom().loadKeyMaterial(clientCert.toFile(), clientCertPassword, clientCertPassword, (aliases, socket) -> aliases.keySet().iterator().next()).build();
	    config.getUserProperties().put("SSL_CONTEXT", sslContext);
	    
	    return config;
	}
	
	public void connect ()
	{
		URI endpointUri = null;
		try {
			endpointUri = new URI("wss://172.26.37.15:2223/pdp/notifications");
		} catch (URISyntaxException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		WebSocketContainer container = ContainerProvider.getWebSocketContainer();
		SSLContext context = null;
		try {
			context = SSLContext.getInstance("TLSv1.2");
			context.init(null, null, null);
		} catch (NoSuchAlgorithmException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (KeyManagementException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			//container.connectToServer(this, endpointUri);
			/*container.connectToServer(new Endpoint() {
				@Override
				public void onOpen(Session session, EndpointConfig endpointConfig)
				{
					
				}
			}, this.createClientConfig(context), endpointUri);*/
			//container.connectToServer(PFApi.class, this.createClientConfig(context), endpointUri);
			container.connectToServer(this.wsClient, this.createClientConfig(context), endpointUri);
		} catch (DeploymentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
/*	
	@OnOpen
	public void onOpen (Session session, EndpointConfig config)
	{
		logger.info("Open Session: " + session.getId());
		this.session = session;
	}
	
	@OnClose
	public void onClose (Session session, CloseReason reason)
	{
		logger.info("Close Session: " + session.getId() + " / Reason: " + reason.getReasonPhrase());
		this.session = null;
	}
	
	@OnMessage
	public void onMessage (String message)
	{
		logger.info("Message received: " + message);
	}
*/
}
