package edu.upc.gco.slcnt.websocket.client.pf.javax;

import javax.websocket.CloseReason;
import javax.websocket.Endpoint;
import javax.websocket.EndpointConfig;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.Session;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class PFClient extends Endpoint {

	private Log logger = LogFactory.getLog(PFClient.class);
	Session session;
	
	public PFClient()
	{
		super();
	}
	
	@Override
	public void onOpen(Session session, EndpointConfig config) {
		// TODO Auto-generated method stub
		logger.info("Open Session: " + session.getId());
		this.session = session;
	}
	
	@OnClose
	public void onClose (Session session, CloseReason reason)
	{
		logger.info("Close Session: " + session.getId() + " / Reason: " + reason.getReasonPhrase());
		this.session = null;
	}
	
	@OnMessage
	public void onMessage (String message)
	{
		logger.info("Message received: " + message);
	}
}
