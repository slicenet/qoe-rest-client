package edu.upc.gco.slcnt.pf.client.rest.tal.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Diagnosis {

	@JsonProperty("symptom")
	private Symptom symptom;
	
	@JsonProperty("causes")
	private Causes causes;

	public Symptom getSymptom() {
		return symptom;
	}

	public void setSymptom(Symptom symptom) {
		this.symptom = symptom;
	}

	public Causes getCauses() {
		return causes;
	}

	public void setCauses(Causes causes) {
		this.causes = causes;
	}

	@Override
	public String toString() {
		return "[symptom=" + symptom + ", causes=" + causes + "]";
	}
}
