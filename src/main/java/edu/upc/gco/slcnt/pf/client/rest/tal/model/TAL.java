package edu.upc.gco.slcnt.pf.client.rest.tal.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TAL {

	@JsonProperty("tal")
	private List<TALRule> talRules;

	public List<TALRule> getTalRules() {
		return talRules;
	}

	public void setTalRules(List<TALRule> talRules) {
		this.talRules = talRules;
	}

	@Override
	public String toString() {
		return "[talRules=" + talRules + "]";
	}
}
