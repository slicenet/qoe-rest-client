package edu.upc.gco.slcnt.pf.client.rest.onap.model.policy;

import com.fasterxml.jackson.annotation.JsonProperty;

import edu.upc.gco.slcnt.pf.client.rest.tal.model.TAL;

public class Config {

	@JsonProperty("service")
	private String service = null;
	
	@JsonProperty("location")
	private String location = null;
	
	@JsonProperty("uuid")
	private String uuid = null;
	
	@JsonProperty("policyName")
	private String policyName = null;
	
	@JsonProperty("description")
	private String description = null;
	
	@JsonProperty("configName")
	private String configName = null;
	
	@JsonProperty("templateVersion")
	private String templateVersion = null;
	
	@JsonProperty("version")
	private String version = null;
	
	@JsonProperty("priority")
	private String priority = null;
	
	@JsonProperty("policyScope")
	private String policyScope = null;
	
	@JsonProperty("riskType")
	private String riskType = null;
	
	@JsonProperty("riskLevel")
	private String riskLevel = null;
	
	@JsonProperty("guard")
	private String guard = null;
	
	@JsonProperty("content")
	private TAL content = null;

	public TAL getContent() {
		return content;
	}

	@Override
	public String toString() {
		return "{service=" + service + ", location=" + location + ", uuid=" + uuid + ", policyName=" + policyName
				+ ", description=" + description + ", configName=" + configName + ", templateVersion=" + templateVersion
				+ ", version=" + version + ", priority=" + priority + ", policyScope=" + policyScope + ", riskType="
				+ riskType + ", riskLevel=" + riskLevel + ", guard=" + guard + ", content=" + content + "}";
	}
}
