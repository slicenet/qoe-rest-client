package edu.upc.gco.slcnt.pf.client.rest.tal.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Action {

	@JsonProperty("actionOption")
	private List<ActionOption> actionOptions;
	
	@JsonProperty("order")
	private String order;
	
	@JsonProperty("id")
	private String id;

	public List<ActionOption> getActionOptions() {
		return actionOptions;
	}

	public void setActionOptions(List<ActionOption> actionOptions) {
		this.actionOptions = actionOptions;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Action [actionOptions=" + actionOptions + ", order=" + order + ", id=" + id + "]";
	}
}
