package edu.upc.gco.slcnt.pf.client.rest.onap.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Policy {

	@JsonProperty("policyConfigMessage")
	private String policyConfigMessage = null;
	
	@JsonProperty("policyConfigStatus")
	private String policyConfigStatus = null;
	
	@JsonProperty("type")
	private String type = null;		//JSON, OTHER
	
	@JsonProperty("config")
	private String config = null;
	
	@JsonProperty("policyName")
	private String policyName = null;
	
	@JsonProperty("policyType")
	private String policyType = null;	//MicroService, BRMS_PARAM
	
	@JsonProperty("policyVersion")
	private String policyVersion = null;
	
	@JsonProperty("matchingConditions")
	private MatchingConditions matchingConditions = null;
	
	@JsonProperty("responseAttributes")
	private ResponseAttributes responseAttributes = null;
	
	//TODO: Check type. Should it be a class?
	@JsonProperty("property")
	private String property = null;
	
	public String getPolicyName() {
		return policyName;
	}

	public String getType() {
		return type;
	}

	public MatchingConditions getMatchingConditions() {
		return matchingConditions;
	}

	public String getConfig() {
		return config;
	}

	@Override
	public String toString()
	{
		String str = "";
		
		str += " { policyConfigMessage=" + this.policyConfigMessage + ", policyConfigStatus=" + this.policyConfigStatus + ", type=" + this.type + ", config=" + this.config + ", policyName=" + this.policyName + ", policyType=" + this.policyType + ((this.matchingConditions == null) ? "":this.matchingConditions.toString()) + " } ";
		
		return str;
	}
}
