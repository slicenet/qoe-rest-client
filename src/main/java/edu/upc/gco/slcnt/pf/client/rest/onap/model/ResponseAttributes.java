package edu.upc.gco.slcnt.pf.client.rest.onap.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResponseAttributes {

	@JsonProperty("controller")
	private String controller = null;
	
	@Override
	public String toString()
	{
		String str = "";
		
		str += " { controller=" + this.controller + " } ";
		
		return str;
	}
}
