package edu.upc.gco.slcnt.pf.client.rest.tal.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ActionOption {

	@JsonProperty("actuator")
	private Actuator actuator;
	
	@JsonProperty("id")
	private String id;
	
	@JsonProperty("operation")
	private String operation;
	
	@JsonProperty("description")
	private String description;
	
	@JsonProperty("typeOfOption")
	private String typeOfOption;

	public Actuator getActuator() {
		return actuator;
	}

	public void setActuator(Actuator actuator) {
		this.actuator = actuator;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTypeOfOption() {
		return typeOfOption;
	}

	public void setTypeOfOption(String typeOfOption) {
		this.typeOfOption = typeOfOption;
	}

	@Override
	public String toString() {
		return "[actuator=" + actuator + ", id=" + id + ", operation=" + operation + ", description="
				+ description + ", typeOfOption=" + typeOfOption + "]";
	}
}
