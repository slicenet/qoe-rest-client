package edu.upc.gco.slcnt.pf.client.websocket;

import java.io.IOException;
import java.net.URI;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;

import com.fasterxml.jackson.databind.ObjectMapper;

import edu.upc.gco.slcnt.pf.client.websocket.model.Notification;

public class PFClientWS extends WebSocketClient {

	private Log logger = LogFactory.getLog(PFClientWS.class);
	private NotificationCB notificationCB;
	
	
	public PFClientWS(URI serverUri, NotificationListener notificationListener) {
		super(serverUri);
		this.notificationCB = notificationListener;
		registerNotificationCB (this.notificationCB);
	}
	
	public void registerNotificationCB (NotificationCB notificationCB)
	{
		this.notificationCB = notificationCB;
	}
	
	@Override
	public void onClose(int code, String reason, boolean remote) {
		// TODO Auto-generated method stub
		logger.info("Disconnected: " + code + " / " + reason + " / " + remote);
	}

	@Override
	public void onError(Exception arg0) {
		// TODO Auto-generated method stub
		logger.error("Error: " + arg0.getMessage());
	}

	@Override
	public void onMessage(String message) {
		// TODO Auto-generated method stub
		logger.debug("Msg: " + message);
		
		ObjectMapper mapper = new ObjectMapper();
		Notification notification = null;
		try {
			notification = mapper.readValue(message, Notification.class);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.notificationCB.notificationUpdate(notification);
	}

	@Override
	public void onOpen(ServerHandshake arg0) {
		// TODO Auto-generated method stub
		logger.info("Connected: " + arg0.getHttpStatus());
	}

}
