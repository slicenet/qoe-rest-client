package edu.upc.gco.slcnt.pf.client.websocket.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Policy {

	@JsonProperty("policyName")
	private String policyName = null;
	
	@JsonProperty("versionNo")
	private String versionNo = null;
	
	@JsonProperty("matches")
	private Matches matches = null;
	
	@JsonProperty("updateType")
	private String updateType = null;	// UPDATE or NEW (null for removed?)

	public String getPolicyName() {
		return policyName;
	}

	public void setPolicyName(String policyName) {
		this.policyName = policyName;
	}

	public String getVersionNo() {
		return versionNo;
	}

	public void setVersionNo(String versionNo) {
		this.versionNo = versionNo;
	}

	public Matches getMatches() {
		return matches;
	}

	public void setMatches(Matches matches) {
		this.matches = matches;
	}

	public String getUpdateType() {
		return updateType;
	}

	public void setUpdateType(String updateType) {
		this.updateType = updateType;
	}
	
	@Override
	public String toString()
	{
		String str = "";
		
		str += " { policyName=" + this.policyName + ", versionNo=" + this.versionNo + ((this.matches == null) ? "":", " + this.matches.toString()) + ((this.updateType == null) ? "":", " + this.updateType) + " } ";
		
		return str;
	}
}
