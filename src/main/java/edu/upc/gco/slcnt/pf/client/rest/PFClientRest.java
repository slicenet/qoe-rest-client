package edu.upc.gco.slcnt.pf.client.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import edu.upc.gco.slcnt.pf.client.rest.onap.model.Policy;
import edu.upc.gco.slcnt.pf.client.rest.onap.model.PolicyRequest;

public abstract class PFClientRest {

	protected Log logger = LogFactory.getLog(PFClientRest.class);
	protected String sliceId, ipAddress, port, protocol;
	protected RestTemplate restTemplate;
	// Transform to Abstract
	//protected HttpComponentsClientHttpRequestFactory requestFactory;
	
	final protected String pfRest = "/pdp/api";
	
	public PFClientRest (String sliceId, String ipAddress, String port, String protocol)
	{
		this.sliceId = sliceId;
		this.ipAddress = ipAddress;
		this.port = port;
		this.protocol = protocol;
		
		// Transform to Abstract
		/*requestFactory = Utils.getSslDisabledClientHttpRequestFactory();
		restTemplate = new RestTemplate(requestFactory);*/
	}
	
	public Policy getPolicy (String policyName, String policyVersion)
	{
		// Transform to Abstract
		/*
		String url = protocol + "://" + ipAddress + ":" + port + pfRest + "/getConfig";
		logger.info("URL= " + url);
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		List<MediaType> mediaTypes = new ArrayList<MediaType>();
		mediaTypes.add(MediaType.APPLICATION_JSON);
		headers.setAccept(mediaTypes);
		headers.set("ClientAuth","cHl0aG9uOnRlc3Q=");
		headers.set("Authorization","Basic dGVzdHBkcDphbHBoYTEyMw==");
		headers.set("Environment","TEST");
		
		PolicyRequest policyRequest = new PolicyRequest();
		policyRequest.setPolicyName(policyName);
		policyRequest.setRequestID(UUID.randomUUID());
		HttpEntity<PolicyRequest> request = new HttpEntity<PolicyRequest>(policyRequest,headers);
		ResponseEntity<List<Policy>> response = restTemplate.exchange(url, HttpMethod.POST, request,new ParameterizedTypeReference<List<Policy>>() {});
		logger.info("Status = " + response.getStatusCodeValue());
		
		// There should be just one element in the list!
		List<Policy> slicePolicies = new ArrayList<Policy>();
    	for (Policy p : response.getBody())
		{
    		if (p.getMatchingConditions() != null && p.getMatchingConditions().getUuid() != null && p.getMatchingConditions().getUuid().equals(sliceId))
			{
    			logger.info("Adding Policy: " + p.getPolicyName());
				slicePolicies.add(p);
			}
		}
		
    	return slicePolicies.get(0);
    	*/
		return null;
	}
	
	public List<Policy> getPolicies ()
	{
		// Transform to Abstract
		/*String url = protocol + "://" + ipAddress + ":" + port + pfRest + "/getConfig";
		logger.info("URL= " + url);
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		List<MediaType> mediaTypes = new ArrayList<MediaType>();
		mediaTypes.add(MediaType.APPLICATION_JSON);
		headers.setAccept(mediaTypes);
		headers.set("ClientAuth","cHl0aG9uOnRlc3Q=");
		headers.set("Authorization","Basic dGVzdHBkcDphbHBoYTEyMw==");
		headers.set("Environment","TEST");
		
		PolicyRequest policyRequest = new PolicyRequest();
		policyRequest.setPolicyName(".*");
		policyRequest.setRequestID(UUID.randomUUID());
		HttpEntity<PolicyRequest> request = new HttpEntity<PolicyRequest>(policyRequest,headers);
		ResponseEntity<List<Policy>> response = restTemplate.exchange(url, HttpMethod.POST, request,new ParameterizedTypeReference<List<Policy>>() {});
		logger.info("Status = " + response.getStatusCodeValue());
		
		List<Policy> slicePolicies = new ArrayList<Policy>();
    	for (Policy p : response.getBody())
		{
    		if (p.getMatchingConditions() != null && p.getMatchingConditions().getUuid() != null && p.getMatchingConditions().getUuid().equals(sliceId))
			{
    			logger.info("Adding Policy: " + p.getPolicyName());
				slicePolicies.add(p);
			}
		}
		
    	return slicePolicies;*/
		return null;
	}
}
