package edu.upc.gco.slcnt.pf.client.rest.tal.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Parameter {

	@JsonProperty("OID")
	private String OID;
	
	@JsonProperty("name")
	private String name;
	
	@JsonProperty("description")
	private String description;
	
	@JsonProperty("value")
	private String value;

	public String getOID() {
		return OID;
	}

	public void setOID(String oID) {
		OID = oID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "[OID=" + OID + ", name=" + name + ", description=" + description + ", value=" + value + "]";
	}
}
