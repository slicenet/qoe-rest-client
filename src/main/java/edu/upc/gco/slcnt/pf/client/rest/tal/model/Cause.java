package edu.upc.gco.slcnt.pf.client.rest.tal.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Cause {

	@JsonProperty("likelihood")
	private String likelihood;
	
	@JsonProperty("causeOID")
	private String causeOid;
	
	@JsonProperty("description")
	private String description;

	public String getLikelihood() {
		return likelihood;
	}

	public void setLikelihood(String likelihood) {
		this.likelihood = likelihood;
	}

	public String getCauseOid() {
		return causeOid;
	}

	public void setCauseOid(String causeOid) {
		this.causeOid = causeOid;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "[likelihood=" + likelihood + ", causeOid=" + causeOid + ", description=" + description + "]";
	}
}
