package edu.upc.gco.slcnt.pf.client.rest.tal.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TALRule {

	@JsonProperty("bootstrap")
	private Bootstrap bootstrap;
	
	@JsonProperty("reaction")
	private Reaction reaction;

	public Bootstrap getBootstrap() {
		return bootstrap;
	}

	public void setBootstrap(Bootstrap bootstrap) {
		this.bootstrap = bootstrap;
	}

	public Reaction getReaction() {
		return reaction;
	}

	public void setReaction(Reaction reaction) {
		this.reaction = reaction;
	}

	@Override
	public String toString() {
		return "[bootstrap=" + bootstrap + ", reaction=" + reaction + "]";
	}
}
